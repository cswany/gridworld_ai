using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour
{

    private float horizMove;
    private float vertMove;

    public float moveSpeed;
    public float scrollSpeed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        horizMove = Input.GetAxis("Horizontal") * Time.deltaTime * moveSpeed;
        vertMove = Input.GetAxis("Vertical") * Time.deltaTime * moveSpeed;

        transform.Translate(horizMove, vertMove, 0f);

        gameObject.GetComponent<Camera>().orthographicSize = gameObject.GetComponent<Camera>().orthographicSize + Input.mouseScrollDelta.y * Time.deltaTime * -scrollSpeed;
    }
}
