using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CellController : MonoBehaviour
{

    //Terrain declarations
    public Sprite[] textureList;
    public SpriteRenderer spriteRenderer;
    public enum terrainTypeEnum
    {
        Grass,
        Water,
        Rock
    }
    public terrainTypeEnum terrainType;
    public int terrainMagnitude; //An int associated with the terrain type that represents different things for each kind of terrain
                                 //In the case of 'Grass', it represents how tall the grass is

    //Grid declarations

    public GridContainer gridContainer;

    public int xCoord;
    public int yCoord;

    public bool walkable; 

    //Unit declarations
    public GameObject currentUnit; //Unit currently occupying this cell
    public AgentController currentUnitScript; //Script of unit currently occupying this cell

    //Dijkstra's declarations

    public int dijCost = 1;
    public int dijDistance;
    public bool dijChecked;
    public List<GameObject> checkList = new List<GameObject>();

    public int distanceToBeat;
    public GameObject cellToBeat;

    //A* declarations
    private List<GameObject> starNeighbors = new List<GameObject>();

    private AStarCell currentStats = new AStarCell();

    void Start()
    {
        StartCoroutine("Grow");
    }

    void Update()
    {
        //UpdateTexture();
    }

    void OnMouseDown()
    {
        if (gridContainer.awaitingTargetCell == true)
        {
            gridContainer.DijTargetCellSelected(this);
            Debug.Log(gameObject + " was clicked");
            gridContainer.awaitingTargetCell = false;
        }
    }

    public List<GameObject> GetAStarNeighbors()
    {
        starNeighbors.Clear();

        if (xCoord > 1)
        {
            starNeighbors.Add(gridContainer.GetCellByCoordinate(xCoord - 1, yCoord));
        }
        if (yCoord > 1)
        {
            starNeighbors.Add(gridContainer.GetCellByCoordinate(xCoord, yCoord - 1));
        }
        if (xCoord < gridContainer.gridSizeHorizontal - 1)
        {
            starNeighbors.Add(gridContainer.GetCellByCoordinate(xCoord + 1, yCoord));
        }
        if (yCoord < gridContainer.gridSizeVertical - 1)
        {
            starNeighbors.Add(gridContainer.GetCellByCoordinate(xCoord, yCoord + 1));
        }

        return starNeighbors;  
    }

    public AStarCell GetAStarStats(int starX, int starY)
    {
        currentStats.f = dijCost;
        currentStats.h = Mathf.Abs(starX - xCoord) + Mathf.Abs(starY - yCoord);
        currentStats.g = currentStats.f + currentStats.h;

        return currentStats;
    }

    private void GetNeighbors()
    {
        checkList.Clear();
        if (xCoord > 1)
        {
            checkList.Add(gridContainer.GetCellByCoordinate(xCoord - 1, yCoord));
        }
        if (yCoord > 1)
        {
            checkList.Add(gridContainer.GetCellByCoordinate(xCoord, yCoord - 1));
        }
        if (xCoord < gridContainer.gridSizeHorizontal - 1)
        {
            checkList.Add(gridContainer.GetCellByCoordinate(xCoord + 1, yCoord));
        }
        if (yCoord < gridContainer.gridSizeVertical - 1)
        {
            checkList.Add(gridContainer.GetCellByCoordinate(xCoord, yCoord + 1));
        }        

        /*foreach (GameObject cell in gridContainer.cells)
        {
            if (cell == gameObject)
            {
                //do not add
            }
            else if (Mathf.Abs(cell.GetComponent<CellController>().xCoord - xCoord) + Mathf.Abs(cell.GetComponent<CellController>().yCoord - yCoord) <= 1)
            {
                checkList.Add(cell);
            }
        }*/
    }

    public void CheckNeighbors()
    {
        GetNeighbors();
        
        foreach (GameObject cell in checkList) //For every adjacent cell...
        {
            if (!cell.GetComponent<CellController>().dijChecked) //...if it hasn't been checked yet...
            {
                if (cell.GetComponent<CellController>().currentUnit == null)
                {
                    cell.GetComponent<CellController>().dijDistance = gridContainer.currentDijDistance + cell.GetComponent<CellController>().dijCost;
                    cell.GetComponent<CellController>().dijChecked = true;
                }
                else
                {
                    cell.GetComponent<CellController>().dijDistance = 999;
                    cell.GetComponent<CellController>().dijChecked = true;
                }
            }
        }
        //Check all neighboring cells and, if dijChecked is false on them, calculate their distance by adding their cost to your distance, then set them to checked
    }

    public GameObject GetNextPathLink()
    {
        GetNeighbors();

        distanceToBeat = 0;
        
        foreach (GameObject cell in checkList) //For every adjacent cell...
        {
            if (gridContainer.dijPath.Contains(cell))
            {
                //do not consider this cell, you've already got it on your path
            }
            else
            {
                if (distanceToBeat == 0)
                {
                    distanceToBeat = cell.GetComponent<CellController>().dijDistance;
                    cellToBeat = cell;
                }
                else if (cell.GetComponent<CellController>().dijDistance < distanceToBeat)
                {
                    distanceToBeat = cell.GetComponent<CellController>().dijDistance;
                    cellToBeat = cell;
                }
            }
            
        }

        return cellToBeat;
        //Get the neighbor that has a dijDistance equal to the currentPathDistance
    }
    private IEnumerator Grow()
    {
        yield return new WaitForSeconds(UnityEngine.Random.Range(10f, 20f));

        if (terrainMagnitude < 5)
        {
            terrainMagnitude++;
        }
        UpdateTexture();


        StartCoroutine("Grow");

    }

    public bool AreYouOccupied()
    {
        if (currentUnit == null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public bool AreYouWalkable()
    {
        if (walkable)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public GameObject GetOccupyingUnit()
    {
        if (currentUnit != null)
        {
            return currentUnit;
        }
        else
        {
            return null;
        }
    }
    public void UpdateTexture() //Make sure that the currently displayed sprite matches the conditions of the terrain
    {
        if (!walkable)
        {
            spriteRenderer.sprite = textureList[4];
            dijCost = 999;
            return;
        }
        if (terrainType == terrainTypeEnum.Grass)
        {
            if (terrainMagnitude >= 4)
            {
                spriteRenderer.sprite = textureList[0];
                dijCost = 4;
            }
            else if (terrainMagnitude <= 3 && terrainMagnitude >= 2)
            {
                spriteRenderer.sprite = textureList[1];
                dijCost = 3;
            }
            else if (terrainMagnitude == 1)
            {
                spriteRenderer.sprite = textureList[2];
                dijCost = 2;
            }
            else
            {
                spriteRenderer.sprite = textureList[3];
                dijCost = 1;
            }
        }
        
        //If I add more terrain types later, I'll add their texture rules here. 
    }

    public GameObject GetUnit()
    {
        return currentUnit;
    }

    public int GetTerrainMagnitude()
    {
        return terrainMagnitude;
    }

    public terrainTypeEnum GetTerrainType()
    {
        return terrainType;
    }

    public void Register(GameObject unit)
    {
        currentUnit = unit;
        currentUnitScript = unit.GetComponent<AgentController>();
    }

    public void DeRegister()
    {
        currentUnit = null;
        currentUnitScript = 
        null;
    }

    public void SetCost(int cost)
    {
        dijCost = cost;
    }

    public void SetDistance(int distance)
    {
        dijDistance = distance;
        dijChecked = true;
    }

    public void DijReset()
    {
        dijChecked = false;
    }

    public int GetDijDistance()
    {
        return dijDistance;
    }

    public int GetDijCost()
    {
        return dijCost;
    }



    //public void Register(GameObject unit)
    //{whenever a unit decides to move onto this cell, it registers, which prevents other units from also deciding to move there}
    //the cell stores a reference to the unit and can always pass along information and commands to that unit's script
    
    //public void MateWith(GameObject initiatingUnit)
    //send a request to the registered unit to be mated with

    //public void DealDamage(GameObject initiatingUnit, int amount)
    //deals damage to the registered unit


}
