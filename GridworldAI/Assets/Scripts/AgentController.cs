using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentController : MonoBehaviour
{
    //Sprite declarations
    public SpriteRenderer spriteRenderer;

    public List<Sprite> spriteList = new List<Sprite>();

    public bool lastMoveWasLeft = true;

    public enum unitTypeEnum
    {
        Sheep,
        Wolf
    }

    public unitTypeEnum unitType;

    public int health;
    public int currentHealth;
    public int hunger;

    public enum mentalStateEnum
    {
        Happy,
        Agitated,
        Panicked
    }

    public mentalStateEnum mentalState;

    //Sense declarations

    public int perceptionRange = 10;

    public int urgentRange = 4;

    public GameObject[] allUnits;

    public List<GameObject> unitsInRange = new List<GameObject>();
    public List<GameObject> nearbySheep = new List<GameObject>();
    public List<GameObject> adjacentSheep = new List<GameObject>();
    private List<GameObject> availableMoveList = new List<GameObject>();

    private Vector3 flockingVector;
    private Vector3 fleeingVector;
    private GameObject bestFlockingCell;
    private GameObject bestEscapeCell;
    public GameObject targetCell;

    public bool canIEatHere;

    //Other declarations

    public GridContainer gridContainer;

    public int xCoord;
    public int yCoord;
    
    public GameObject currentCell;
    public CellController currentCellScript;

    //Dij declarations

    public bool dijControlled = false;

    public enum moveStateEnum
    {
        Pathing,
        Idle
    }

    public moveStateEnum moveState;

    public List<GameObject> movePath = new List<GameObject>();

    private bool readyToMoveAlongPath;

    //A* declarations

    public bool aStarControlled = false;

    public Dictionary<GameObject, AStarCell> openList = new Dictionary<GameObject, AStarCell>();
    public Dictionary<GameObject, AStarCell> closedList = new Dictionary<GameObject, AStarCell>();

    private List<GameObject> starCheckList = new List<GameObject>();

    private CellController starTarget;

    private int starCounter;

    private KeyValuePair<GameObject, AStarCell> winningStarCell;

    private List<GameObject> aStarFinalPath = new List<GameObject>();

    //Utility AI declarations

    private float hungerMotivation;
    private float totalHunger;
    
    private float feverMotivation;
    private float currentFever;
    private float totalFever;

    private float runMotivation;

    public Dictionary<GameObject, UtilityCell> utilityList = new Dictionary<GameObject, UtilityCell>();


    // Start is called before the first frame update
    void Start()
    {

    }

    //********************
    //********************
    //********************
    //********************
    //********************
    //Basic functions

    void Update()
    {
        //UpdateTexture();

        switch (moveState)
        {
            case moveStateEnum.Pathing:

            if (readyToMoveAlongPath == true)
            {
                readyToMoveAlongPath = false;

                Debug.Log("move path count is " + movePath.Count);

                if (movePath.Count == 0)
                {
                    Debug.Log("break pathing");
                    moveState = moveStateEnum.Idle;
                    DeRegister();
                    Register(gridContainer.GetCellByCoordinate(xCoord, yCoord));
                }
                
                else
                {
                    transform.position = movePath[movePath.Count - 1].GetComponent<Transform>().position - new Vector3(0f, 0f, 1f);
                    xCoord = movePath[movePath.Count - 1].GetComponent<CellController>().xCoord;
                    yCoord = movePath[movePath.Count - 1].GetComponent<CellController>().yCoord;
                    movePath.Remove(movePath[movePath.Count - 1]);
                    StartCoroutine("MoveWait");
                }

            }
            break;
            
            case moveStateEnum.Idle:
            break;
        }
    }

    public void InitialRegister()
    {
        currentHealth = health;

        moveState = moveStateEnum.Idle;

        currentCell = gridContainer.GetCellByCoordinate(xCoord, yCoord);
        currentCellScript = currentCell.GetComponent<CellController>();

        currentCellScript.Register(gameObject);

        if (!dijControlled && !gridContainer.aStarSetup)
        {
            StartCoroutine("Idle");
        }
    }

    void OnMouseDown()
    {
        if (dijControlled || gridContainer.aStarSetup)
        {
            Debug.Log("unit selected");
            gridContainer.DijUnitSelected(gameObject);
        }
    }

    public void TakePath(List<GameObject> pathList)
    {
        movePath = pathList;
        moveState = moveStateEnum.Pathing;
        movePath.Remove(movePath[movePath.Count - 1]); //for now remove the starting square...
        readyToMoveAlongPath = true;
    }


    public void UpdateTexture()
    {
        if (unitType == unitTypeEnum.Sheep)
        {
            if (mentalState == mentalStateEnum.Happy)
            {
                spriteRenderer.sprite = spriteList[0];
            }
            if (mentalState == mentalStateEnum.Agitated)
            {
                spriteRenderer.sprite = spriteList[1];
            }
            if (mentalState == mentalStateEnum.Panicked)
            {
                spriteRenderer.sprite = spriteList[2];
            }
        }
        else if (unitType == unitTypeEnum.Wolf)
        {
            if (currentHealth >= (0.75 * health))
            {
                spriteRenderer.sprite = spriteList[3];
            }
            else
            {
                spriteRenderer.sprite = spriteList[4];
            }
        }

        if (lastMoveWasLeft)
        {
            Vector3 lTemp = transform.localScale;
            lTemp.x = 0.1f;
            transform.localScale = lTemp;
        }
        else
        {
            Vector3 lTemp = transform.localScale;
            lTemp.x = -0.1f;
            transform.localScale = lTemp;
        }
    }

    //********************
    //********************
    //********************
    //********************
    //********************
    //A* methods

    //Once a unit and square have been selected, gridcontainer must send them to the A* function. (Check)

    //First, add all adjacent squares to the open list, then check for the lowest cost. 

    //Add the lowest cost to the closed list; have the dictionary include cell + cell's parent. 

    //MAIN LOOP:

        //Go through the closed list and add all adjacent non-closed cells to the open list
        //calculate costs (and put 'em in the list!)
        //Add the lowest cost square to the closed list

        //IF the target square is the chosen square, break the loop, and you now have your list. 

    //Then just send the list to the preexisting move function, I think?
    


    public void AStarPathTo(CellController tarCell) //Use A* to get to this cell
    {
        Debug.Log("AStarPath to " + tarCell);

        //Set the target
        starTarget = tarCell;
        
        //Reset everything
        starCounter = 0;
        openList.Clear();
        closedList.Clear();
        starCheckList.Clear();

        AStarLoop(); //begin the pathfinding loop
    }
    
    private void AStarLoop()
    {
        if (starCounter == 0) //If this is our first time through the loop...
        {
            starCheckList.AddRange(currentCellScript.GetAStarNeighbors()); //Gather up adjacent cells

            foreach (GameObject cell in starCheckList) //Calculate each cell's f, g, and h and then add it to the open list
            {
                openList.Add(cell, cell.GetComponent<CellController>().GetAStarStats(starTarget.xCoord, starTarget.yCoord));
                openList[cell].parentCell = currentCell;
            }

            int i = 0;

            foreach (KeyValuePair<GameObject, AStarCell> cell in openList) //Now get the lowest-cost cell on the open list. 
            {
                if (i == 0)
                {
                    winningStarCell = cell;
                }
                else
                {
                    if (cell.Value.g < winningStarCell.Value.g)
                    {
                        winningStarCell = cell;
                    }
                }
                i++;
            }

            closedList.Add(winningStarCell.Key, winningStarCell.Value); //Add that winning cell to the closed list...
            openList.Remove(winningStarCell.Key); //...and take it off the open list. 

            if (winningStarCell.Key.GetComponent<CellController>() == starTarget) //If we've reached the target...
            {
                AStarTakePath(); //Break the loop and build the path!
            }
            else //If we haven't reached the target...
            {
                starCounter++; 

                AStarLoop(); //Do the loop again             
            }

        }
        else //If this is NOT our first time through the loop...
        {
            foreach (KeyValuePair<GameObject, AStarCell> closedCell in closedList) //Look at each cell in the closed list...
            {
                starCheckList.Clear();

                starCheckList.AddRange(closedCell.Key.GetComponent<CellController>().GetAStarNeighbors()); //...and gather all neighbors. 

                foreach (GameObject cell in starCheckList) //For each neighbor...
                {
                    if (openList.ContainsKey(cell)) //If that neighbor is on the open list...
                    {
                        //Don't touch it
                    }
                    else if (closedList.ContainsKey(cell)) //If that neighbor is on the closed list...
                    {
                        //Don't touch it
                    }
                    else //If it's on neither list....
                    {
                        openList.Add(cell, cell.GetComponent<CellController>().GetAStarStats(starTarget.xCoord, starTarget.yCoord)); //Add it to the open list
                        openList[cell].parentCell = closedCell.Key; //...and set its parent to the closed cell that added it
                    }
                }
            }
            int i = 0;

            foreach (KeyValuePair<GameObject, AStarCell> cell in openList) //Now get the lowest-cost cell on the open list.
            {
                if (i == 0)
                {
                    winningStarCell = cell;
                }
                else
                {
                    if (cell.Value.g < winningStarCell.Value.g)
                    {
                        winningStarCell = cell;
                    }
                }
                i++;
            }
            
            closedList.Add(winningStarCell.Key, winningStarCell.Value); //Add that cell to the closed list...
            openList.Remove(winningStarCell.Key); //...and take it off the open list

            if (winningStarCell.Key.GetComponent<CellController>() == starTarget) //If we're at the target...
            {
                AStarTakePath(); //Go to it!
            }
            else //If we're NOT at the target...
            {
                starCounter++;

                AStarLoop(); //Continue looping
            }
        }
    }

    private void AStarTakePath() //Once we've reached the target square...
    {
        //Reset everything

        starCounter = 0;
        aStarFinalPath.Clear();
        aStarFinalPath.Add(winningStarCell.Key);

        while (starCounter == 0) //Until we've built the path all the way back to the starting cell....
        {
            aStarFinalPath.Add(closedList[aStarFinalPath[aStarFinalPath.Count - 1]].parentCell); //Add the next cell in the chain of closed cells
            
            if (aStarFinalPath[aStarFinalPath.Count - 1] == currentCell) //If we're back home, break the while loop
            {
                starCounter++;
            }
        }

        TakePath(aStarFinalPath); //Finally, take the path using the Dijkstra method
    }
    
    //********************
    //********************
    //********************
    //********************
    //********************
    //Utility AI

    //Sheep:

        //3 motivations: hunger, wolf danger, and, uh, 'saturday night fever'
        //Food motivation is determined by hunger and incentivizes finding grass to eat
        //Run motivation is determined by number & proximity of wolves and incentivizes running away
        //Mate motivation is determined by a slowly ticking timer and incentivizes picking flowers and courting other sheep


        //GRASS TEST

        //SENSE:
        //Look at surrounding cells for grass

        //DECIDE:
        //If already taking an action and sense delta is low, just keep acting
        //else...
            //CALCULATE motivations
            //If hunger is low, seek out the highest grass non-occupied square
            //If hunger is not low, wander

        //ACT:
        //If seeking grass, chart A* to that block and make that the action
        //If wandering, just move 1 square in a random direction to be unoccupied


    public void UtilitySense()
    {
        //perceptionRange && urgentRange will be used here

        foreach (GameObject cell in gridContainer.cells) //gather a list of cells in perception radius
        {

        }

        //Check to see: (fill in UtilityCell custom class info in the dictionary)
            //Are they occupied by another unit?
            //Has another unit decided to go there?
            //What terrain type & what magnitude? 
            //
    }

    private void UtilityDecide()
    {
        
    }

    private void UtilityAct()
    {
        //pick an action to take at each UtilitySense
    }

    private void UtilityFindFood()
    {

    }

    private void UtilityFindMate()
    {

    }
    
    private void UtilityFindRunPath()
    {

    }


    private void CalculateMotivations() //these are dirt simple and linear, currently
    {
        //hunger motivation = current hunger / total hunger

        //run motivation = 0 if no wolves, 1 if wolf is in sight

        //mate motivation = current fever / total fever
    }





    //********************
    //********************
    //********************
    //********************
    //********************
    //Dijkstra Methods    
    
    
    //Upon being initiated, pick the proper sprite for your type and state

    private IEnumerator MoveWait()
    {
        yield return new WaitForSeconds(0.5f);

        if (gridContainer.aStarSetup)
        {
            UtilitySense();
        }
        readyToMoveAlongPath = true;
    }

    private IEnumerator Idle()
    {   
        if (unitType == unitTypeEnum.Sheep)
        {
            if (mentalState == mentalStateEnum.Happy)
            {
                yield return new WaitForSeconds(UnityEngine.Random.Range(0.8f, 1.2f));
            }
            else if (mentalState == mentalStateEnum.Agitated)
            {
                yield return new WaitForSeconds(UnityEngine.Random.Range(0.6f, 0.9f));
            }
            else if (mentalState == mentalStateEnum.Panicked)
            {
                yield return new WaitForSeconds(UnityEngine.Random.Range(0.2f, 0.5f));
            }
        }

        if (unitType == unitTypeEnum.Wolf)
        {
            if (currentHealth > 0.75 * health)
            {
                yield return new WaitForSeconds(UnityEngine.Random.Range(2f, 5f));
                currentHealth = currentHealth - 3;
            }
            else
            {
                yield return new WaitForSeconds(UnityEngine.Random.Range(0.1f, 0.3f));
                //currentHealth = currentHealth - 1;
            }
        }
        Sense();
    }


    //IEnumerator IdleLoop
    //Coroutine that has the unit waiting around a bit before acting. During this time, it can be damaged or mated with


    private void Sense()
    {

        allUnits = GameObject.FindGameObjectsWithTag("Unit");
        unitsInRange.Clear();
        nearbySheep.Clear();
        adjacentSheep.Clear();

        foreach (GameObject unit in allUnits)
        {
            if (Mathf.Abs(unit.GetComponent<AgentController>().xCoord - xCoord) + Mathf.Abs(unit.GetComponent<AgentController>().yCoord - yCoord) <= perceptionRange)
            {
                unitsInRange.Add(unit);
            }
        }

        foreach (GameObject unit in unitsInRange)
        {
            if (unit.GetComponent<AgentController>().unitType == unitTypeEnum.Sheep)
            {
                nearbySheep.Add(unit);
            }
            if (unit.GetComponent<AgentController>().unitType == unitTypeEnum.Wolf)
            {
                if(unitType == unitTypeEnum.Sheep)
                {
                    mentalState = mentalStateEnum.Agitated;
                    if (Mathf.Abs(unit.GetComponent<AgentController>().xCoord - xCoord) + Mathf.Abs(unit.GetComponent<AgentController>().yCoord - yCoord) <= 5)
                    {
                        mentalState = mentalStateEnum.Panicked;
                    }
                }
            }
        }

        if (unitsInRange.Count == nearbySheep.Count)
        {
            mentalState = mentalStateEnum.Happy;
        }


        //Calculate escape vector if you're a sheep
        else
        {
            foreach (GameObject unit in unitsInRange)
            {
                if (unit.GetComponent<AgentController>().unitType == unitTypeEnum.Wolf)
                {
                    fleeingVector = transform.position - unit.GetComponent<Transform>().position;
                    fleeingVector.Normalize();

                    float vectorTilt2 = Mathf.Abs(fleeingVector.x/fleeingVector.y); //pick the best square to get in that direction

                    if (vectorTilt2 >= 1 && fleeingVector.x > 0 && fleeingVector.y > 0)
                    {
                        bestEscapeCell = gridContainer.GetCellByCoordinate(xCoord+1, yCoord);
                    }
                    else if (vectorTilt2 <= 1 && fleeingVector.x > 0 && fleeingVector.y > 0)
                    {
                        bestEscapeCell = gridContainer.GetCellByCoordinate(xCoord, yCoord+1);
                    }
                    else if (vectorTilt2 >= 1 && fleeingVector.x < 0 && fleeingVector.y > 0)
                    {
                        bestEscapeCell = gridContainer.GetCellByCoordinate(xCoord - 1, yCoord);
                    }
                    else if (vectorTilt2 <= 1 && fleeingVector.x < 0 && fleeingVector.y > 0)
                    {
                        bestEscapeCell = gridContainer.GetCellByCoordinate(xCoord, yCoord+1);
                    }
                    else if (vectorTilt2 >= 1 && fleeingVector.x < 0 && fleeingVector.y < 0)
                    {
                        bestEscapeCell = gridContainer.GetCellByCoordinate(xCoord - 1, yCoord);
                    }
                    else if (vectorTilt2 <= 1 && fleeingVector.x < 0 && fleeingVector.y < 0)
                    {
                        bestEscapeCell = gridContainer.GetCellByCoordinate(xCoord, yCoord-1);
                    }
                    else if (vectorTilt2 >= 1 && fleeingVector.x > 0 && fleeingVector.y < 0)
                    {
                        bestEscapeCell = gridContainer.GetCellByCoordinate(xCoord+1, yCoord);
                    }
                    else if (vectorTilt2 <= 1 && fleeingVector.x > 0 && fleeingVector.y < 0)
                    {
                        bestEscapeCell = gridContainer.GetCellByCoordinate(xCoord, yCoord-1);
                    }
                }
            }
        }

        foreach (GameObject unit in nearbySheep)
        {
            if (Mathf.Abs(unit.GetComponent<AgentController>().xCoord - xCoord) + Mathf.Abs(unit.GetComponent<AgentController>().yCoord - yCoord) < 2)
            {
                adjacentSheep.Add(unit);
            }
        }

        //Is there grass here for eatin'?

        if (currentCellScript.terrainMagnitude > 0)
        {
            canIEatHere = true;
        }
        else
        {
            canIEatHere = false;
        }
        //Next, for flocking...

        flockingVector = new Vector3(0f, 0f, 0f);

        foreach (GameObject unit in nearbySheep)
        {
            flockingVector = flockingVector + (transform.position - unit.GetComponent<Transform>().position);    //create a vector that averages the direction of other sheep
        }

        flockingVector.Normalize();
        flockingVector = flockingVector * -1;

        float vectorTilt = Mathf.Abs(flockingVector.x/flockingVector.y); //pick the best square to get in that direction

        if (vectorTilt >= 1 && flockingVector.x > 0 && flockingVector.y > 0)
        {
            bestFlockingCell = gridContainer.GetCellByCoordinate(xCoord+1, yCoord);
        }
        else if (vectorTilt <= 1 && flockingVector.x > 0 && flockingVector.y > 0)
        {
            bestFlockingCell = gridContainer.GetCellByCoordinate(xCoord, yCoord+1);
        }
        else if (vectorTilt >= 1 && flockingVector.x < 0 && flockingVector.y > 0)
        {
            bestFlockingCell = gridContainer.GetCellByCoordinate(xCoord - 1, yCoord);
        }
        else if (vectorTilt <= 1 && flockingVector.x < 0 && flockingVector.y > 0)
        {
            bestFlockingCell = gridContainer.GetCellByCoordinate(xCoord, yCoord+1);
        }
        else if (vectorTilt >= 1 && flockingVector.x < 0 && flockingVector.y < 0)
        {
            bestFlockingCell = gridContainer.GetCellByCoordinate(xCoord - 1, yCoord);
        }
        else if (vectorTilt <= 1 && flockingVector.x < 0 && flockingVector.y < 0)
        {
            bestFlockingCell = gridContainer.GetCellByCoordinate(xCoord, yCoord-1);
        }
        else if (vectorTilt >= 1 && flockingVector.x > 0 && flockingVector.y < 0)
        {
            bestFlockingCell = gridContainer.GetCellByCoordinate(xCoord+1, yCoord);
        }
        else if (vectorTilt <= 1 && flockingVector.x > 0 && flockingVector.y < 0)
        {
            bestFlockingCell = gridContainer.GetCellByCoordinate(xCoord, yCoord-1);
        }




        //log the cells around you that are not occupied

        availableMoveList.Clear();

        if (gridContainer.GetCellByCoordinate(xCoord, yCoord-1).GetComponent<CellController>().AreYouOccupied() == false && gridContainer.GetCellByCoordinate(xCoord, yCoord-1).GetComponent<CellController>().AreYouWalkable() == true)
        {
            availableMoveList.Add(gridContainer.GetCellByCoordinate(xCoord, yCoord-1));
        }
        if (gridContainer.GetCellByCoordinate(xCoord+1, yCoord).GetComponent<CellController>().AreYouOccupied() == false && gridContainer.GetCellByCoordinate(xCoord+1, yCoord).GetComponent<CellController>().AreYouWalkable() == true)
        {
            availableMoveList.Add(gridContainer.GetCellByCoordinate(xCoord+1, yCoord));
        }
        if (gridContainer.GetCellByCoordinate(xCoord, yCoord+1).GetComponent<CellController>().AreYouOccupied() == false && gridContainer.GetCellByCoordinate(xCoord, yCoord+1).GetComponent<CellController>().AreYouWalkable() == true)
        {
            availableMoveList.Add(gridContainer.GetCellByCoordinate(xCoord, yCoord+1));
        }
        if (gridContainer.GetCellByCoordinate(xCoord-1, yCoord).GetComponent<CellController>().AreYouOccupied() == false && gridContainer.GetCellByCoordinate(xCoord-1, yCoord).GetComponent<CellController>().AreYouWalkable() == true)
        {
            availableMoveList.Add(gridContainer.GetCellByCoordinate(xCoord-1, yCoord));
        }

        Decide();
    }

    private void Decide()
    {
        if (unitType == unitTypeEnum.Sheep)
        {
            if (availableMoveList.Count > 0)
            {
                //Decide based on behavior what is best

                if (mentalState == mentalStateEnum.Happy)
                {
                    int rollDie = UnityEngine.Random.Range(0, 100);

                    if (rollDie <= 65) //chance of wandering randomly
                    {
                        targetCell = availableMoveList[UnityEngine.Random.Range((int) 0, availableMoveList.Count)];
                    }

                    else if (rollDie <= 90) //chance of flocking
                    {
                        if (availableMoveList.Contains(bestFlockingCell))
                        {
                            targetCell = bestFlockingCell;
                        }
                        else
                        {
                            targetCell = availableMoveList[UnityEngine.Random.Range((int) 0, availableMoveList.Count)];
                        }
                    }

                    else //chance that you'll either eat or stand around
                    {
                        Eat();
                        return;
                    }
                }
                if (mentalState == mentalStateEnum.Agitated)
                {
                    int rollDie = UnityEngine.Random.Range(0, 100);

                    if (rollDie <= 70) //chance of running
                    {
                        if (availableMoveList.Contains(bestEscapeCell))
                        {
                            targetCell = bestEscapeCell;
                        }
                        else
                        {
                            targetCell = availableMoveList[UnityEngine.Random.Range((int) 0, availableMoveList.Count)];
                        }
                    }

                    else if (rollDie <= 90) //chance of flocking
                    {
                        if (availableMoveList.Contains(bestFlockingCell))
                        {
                            targetCell = bestFlockingCell;
                        }
                        else
                        {
                            targetCell = availableMoveList[UnityEngine.Random.Range((int) 0, availableMoveList.Count)];
                        }
                    }

                    else //chance that you'll either eat or stand around
                    {
                        Eat();
                        return;
                    }
                }
                if (mentalState == mentalStateEnum.Panicked)
                {
                    if (availableMoveList.Contains(bestEscapeCell))
                    {
                        targetCell = bestEscapeCell;
                    }
                    else
                    {
                        targetCell = availableMoveList[UnityEngine.Random.Range((int) 0, availableMoveList.Count)];
                    }
                }
            
                Act();
            }
        


            else
            {
                if (canIEatHere)
                {
                    Eat();
                }
                CheckMating();
                StartCoroutine("Idle");
            }
        }

        if (unitType == unitTypeEnum.Wolf)
        {
            //Debug.Log("wolf deciding");
            if (currentHealth > (0.75 * health))
            {
                int rollDie = UnityEngine.Random.Range(0, 100);

                    if (rollDie <= 50) //chance of wandering
                    {
                        targetCell = availableMoveList[UnityEngine.Random.Range((int) 0, availableMoveList.Count)];
                    }

                    else if (rollDie <= 100) //chance of hunting sheep
                    {
                        if (availableMoveList.Contains(bestFlockingCell))
                        {
                            targetCell = bestFlockingCell;
                        }
                        else
                        {
                            targetCell = availableMoveList[UnityEngine.Random.Range((int) 0, availableMoveList.Count)];
                        }
                    }
            }
            else
            {
                    int rollDie = UnityEngine.Random.Range(0, 100);

                    if (rollDie <= 20) //chance of wandering
                    {
                        targetCell = availableMoveList[UnityEngine.Random.Range((int) 0, availableMoveList.Count)];
                    }

                    else if (rollDie <= 100) //chance of hunting sheep
                    {
                        if (availableMoveList.Contains(bestFlockingCell))
                        {
                            targetCell = bestFlockingCell;
                        }
                        else
                        {
                            targetCell = availableMoveList[UnityEngine.Random.Range((int) 0, availableMoveList.Count)];
                        }
                    }
            }

            Act();

        }
    }

    private void Act()
    {
        if (unitType == unitTypeEnum.Sheep)
        {
            if (targetCell.GetComponent<CellController>().AreYouOccupied() == false)
            {
                Vector3 oldPosition = transform.position;
                transform.position = new Vector3(targetCell.GetComponent<Transform>().position.x, targetCell.GetComponent<Transform>().position.y, -1f);
                
                if (transform.position.x - oldPosition.x < 0)
                {
                    lastMoveWasLeft = true;
                }
                
                else if (transform.position.x - oldPosition.x > 0)
                {
                    lastMoveWasLeft = false;
                }

                UpdateTexture();

                DeRegister();
                Register(targetCell);

                xCoord = targetCell.GetComponent<CellController>().xCoord;
                yCoord = targetCell.GetComponent<CellController>().yCoord;

                CheckMating();
                StartCoroutine("Idle");
            }
            else
            {
                CheckMating();
                StartCoroutine("Idle");
            }
        }
        if (unitType == unitTypeEnum.Wolf)
        {
            if (targetCell.GetComponent<CellController>().AreYouOccupied() == false)
            {
                Vector3 oldPosition = transform.position;
                transform.position = new Vector3(targetCell.GetComponent<Transform>().position.x, targetCell.GetComponent<Transform>().position.y, -1f);
                
                if (transform.position.x - oldPosition.x < 0)
                {
                    lastMoveWasLeft = true;
                }
                
                if (transform.position.x - oldPosition.x > 0)
                {
                    lastMoveWasLeft = false;
                }

                UpdateTexture();

                DeRegister();
                Register(targetCell);

                xCoord = targetCell.GetComponent<CellController>().xCoord;
                yCoord = targetCell.GetComponent<CellController>().yCoord;

                CheckEating();
                StartCoroutine("Idle");
            }
            
            else
            {
                CheckEating();
                StartCoroutine("Idle");
            }
        }
    }

    public void CheckMating()
    {
        int rollDie = UnityEngine.Random.Range(0, 100);
        if (rollDie <= 20)//20% chance child gets created when you're around a partner
        {
            if (currentHealth > (health * 0.75) && adjacentSheep.Count > 0)
            {
                if (availableMoveList.Count > 0)
                {
                    GameObject chosenCell = availableMoveList[UnityEngine.Random.Range((int) 0, availableMoveList.Count)];
                    gridContainer.CreateNewSheepAt(chosenCell.GetComponent<CellController>().xCoord, chosenCell.GetComponent<CellController>().yCoord, currentHealth);
                    currentHealth = currentHealth/2;
                }
            }
        }
        
    }

    public void CheckEating()
    {
        if (adjacentSheep.Count > 0)
        {
            GameObject chosenSheep = adjacentSheep[UnityEngine.Random.Range(0, adjacentSheep.Count)];
            currentHealth += chosenSheep.GetComponent<AgentController>().GetHealth();
            chosenSheep.GetComponent<AgentController>().GetEaten();

            if (currentHealth > health)
            {
                currentHealth = health;
            }
        }
        
    }

    private void Eat()
    {
        if (currentCellScript.terrainMagnitude > 0)
        {
            currentCellScript.terrainMagnitude = currentCellScript.terrainMagnitude - 2;
            currentCellScript.UpdateTexture();
            if (currentHealth < health)
            {currentHealth++;}
        }
        StartCoroutine("Idle");
    }
    
    public int GetHealth()
    {
        return currentHealth;
    }

    public void GetEaten()
    {
        DeRegister();
        Destroy(gameObject);
    }
    private void DeRegister()
    {
        currentCellScript.DeRegister();

        currentCell = null;
        currentCellScript = null;
    }

    private void Register(GameObject cell)
    {
        currentCell = cell;
        currentCellScript = cell.GetComponent<CellController>();

        currentCellScript.Register(gameObject);
    }


    //Dijkstra's Method Planned Code

    //At each step, grab all un-checked orthogonal squares from the target square
        //currentCellList.Clear();
        //currentCellList.Add(gridContainer.GetCellByCoordinate(currentCellX +/- 1, currentCellY +/- 1).GetComponent<CellController>());
        
        //foreach (CellController cell in currentCellList)
            //{
            //if (cell.walkable)
            //{cell.SetCost(currentCost);}
            //else
            //{cell.SetCost(999)}
            //if (cell == targetCell)
            //{BuildPath()}
            //}

        //currentCost++;
        //Now pick a random new target cell at the minimum proper breadth (which I've calculated in code above)

    //BuildPath()
    //
    //As above, but in reverse!
    //Starting at the target cell, grab all orthogonally adjacent cells
        //foreach (CellController cell in adjacentCells)
        //if (cell == homeCell)
        //{FollowPath()}
        //else
        //{add the lowest cell to PathList}
        //targetCell = lowest cell, do the loop again
    
    //FollowPath()
    //Loop through the list
        //At each step, move to PathList[PathList.Count - 1]
        //Then PathList.Remove[PathList.Count - 1]











    //private void Sense()
    //Called by the coroutine. 
    //Checks all squares within a certain radius to see A) what the terrain info is and B) what units are registered there

    //private void Decide()
    //The main AI zone.
    //Based on the sense information, create a sliding percentage scale for the available act options...
    //Then choose one based on that percentage.

    //private void Act()
    //Perform the action set out in Decide().
    //First, check to see if you still *can* (for instance, another unit may've just registered in the zone you were about to move to)
    //If you can still do it, register yourself and perform the action
    //If you can't do it, go back to Sense() and try to pick something else

    //public void ChangeHealth(int amount)
    //Increase or decrease health by a certain amount

    //public void Mate(?)
    //You've been selected to mate! Abandon any actions you're currently performing and spawn a child around you if you can. 
    //If you can't spawn a child, then abandon the endeavor and go back to IdleLoop 
}
