using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridContainer : MonoBehaviour
{
    //Grid declarations

    public GameObject cellPrefab;
    public GameObject unitPrefab;

    public int gridSizeHorizontal = 5; //These two variables are set in the inspector. 
    public int gridSizeVertical = 5; //These two variables are set in the inspector. 

    public GameObject[,] cells;

    public enum unitEnum
    {
        Sheep,
        Wolf
    }

    public List<unitEnum> unitList = new List<unitEnum>();

    public List<int> unitCoordListHoriz = new List<int>();
    public List<int> unitCoordListVert = new List<int>();

    public int sheepPerceptionRange;
    public int wolfPerceptionRange;

    public int sheepUrgentRange;
    public int wolfUrgentRange;

    public int sheepHealth;
    public int wolfHealth = 20;

    //Dij declarations

    public bool dijkstraSetup;

    public bool awaitingTargetCell;

    public GameObject dijUnit;

    public CellController dijTargetCell;

    public CellController dijStartingCell;

    public int currentDijDistance;

    public int currentPathDistance;

    public List<GameObject> dijPath = new List<GameObject>();

    public int extraPerceptionCounter;

    //A* declarations

    public bool aStarSetup;

    void Start()
    {
        cells = new GameObject[gridSizeHorizontal, gridSizeVertical];
        InitializeGrid();
        awaitingTargetCell = true;
    }

    void Update()
    {
        if (Input.GetButtonDown("Fire2")) //right click to cancel unit selection
        {
            awaitingTargetCell = false;
        }
    }

    public void DijUnitSelected(GameObject unit) //Unit clicked; awaiting name of cell they must go to
    {
        dijUnit = unit;
        awaitingTargetCell = true;
        Debug.Log(unit + " selected");
    }

    public void DijTargetCellSelected(CellController tarCell) //A cell and unit have been selected, begin pathfinding
    {
        dijTargetCell = tarCell;
        awaitingTargetCell = false;
        Debug.Log("DijTargetCellSelected");

        if (dijkstraSetup)
        {
            DoDijkstraMethod();
        }
        else if (aStarSetup)
        {
            dijUnit.GetComponent<AgentController>().AStarPathTo(tarCell);
        }
    }

    public void DoDijkstraMethod() //Begin pathfinding! 
    {
        foreach (GameObject cell in cells) //reset the distance value for all cells
        {
            cell.GetComponent<CellController>().dijChecked = false;
            cell.GetComponent<CellController>().dijDistance = 999;
        }

        //Set the starting cell
        dijStartingCell = cells[dijUnit.GetComponent<AgentController>().xCoord, dijUnit.GetComponent<AgentController>().yCoord].GetComponent<CellController>();

        extraPerceptionCounter = 0;
        currentDijDistance = 0;
        Debug.Log("dodij");
        DijkstraLoop();
    }

    private void DijkstraLoop() //Main pathfinding loop
    {
        Debug.Log("Started dij loop");
        if (currentDijDistance == 0) //If this is your first loop, then check the neighbors of the target cell
        {
            Debug.Log("should only happen once");
            dijStartingCell.CheckNeighbors();
            currentDijDistance++;
            DijkstraLoop();
        }
        else //If this is *not* your first loop...
        {
            foreach (GameObject cell in cells) //Check every cell to see if it's at the depth distance we're checking
            {
                if (cell.GetComponent<CellController>().GetDijDistance() == currentDijDistance) //Note to self: this is not the best way to do this. Later, sub in ... .GetDijDistance() - cell.GetComponent<CellController>().dijCost + 1 == currentDijDistance) //
                {
                    cell.GetComponent<CellController>().CheckNeighbors(); //if it is, check its neighbors
                }
            }
            Debug.Log("main dij loop");
            currentDijDistance++;

            if (dijTargetCell.dijChecked) //if you've found the target cell, find the best path!
            {
                dijPath.Clear();
                dijPath.Add(GetCellByCoordinate(dijTargetCell.xCoord, dijTargetCell.yCoord));
                currentPathDistance = currentDijDistance;
                if (Mathf.Abs(dijTargetCell.xCoord - dijStartingCell.xCoord) + Mathf.Abs(dijTargetCell.yCoord - dijStartingCell.yCoord) == 1)
                {
                    dijPath.Add(dijPath[currentDijDistance - currentPathDistance].GetComponent<CellController>().GetNextPathLink());
                    dijUnit.GetComponent<AgentController>().TakePath(dijPath);
                }
                else
                {
                    DijkstraPath(); //build the path!
                }
                
                /*if (extraPerceptionCounter > 10) //I'll use this later when I have more sophisticated sheep perception
                {
                    if (Mathf.Abs(dijTargetCell.xCoord - dijStartingCell.xCoord) + Mathf.Abs(dijTargetCell.yCoord - dijStartingCell.yCoord) == 1)
                    {
                        dijPath.Add(dijPath[currentDijDistance - currentPathDistance].GetComponent<CellController>().GetNextPathLink());
                        dijUnit.GetComponent<AgentController>().TakePath(dijPath);
                    }
                    else
                    {
                        DijkstraPath(); //build the path!
                    }
                }
                else
                {
                    extraPerceptionCounter++;
                    DijkstraLoop();
                }*/

            }
            else
            {
                DijkstraLoop();
            }
        }
    }

    private void DijkstraPath() //Find the best path by using this cell algorithm to find the path of least resistance
    {
        dijPath.Add(dijPath[currentDijDistance - currentPathDistance].GetComponent<CellController>().GetNextPathLink());
        currentPathDistance--;
        Debug.Log("dijPath count is " + dijPath.Count);
        //dijUnit.GetComponent<AgentController>().TakePath(dijPath);
        if (currentPathDistance == 0)
        {
            dijUnit.GetComponent<AgentController>().TakePath(dijPath);
        }
        else
        {
            DijkstraPath();
        }
    }

    private void InitializeGrid()
    {
        for (int i = 0; i < gridSizeHorizontal; i++)
        {
            for (int j = 0; j < gridSizeVertical; j++)
            {
                GameObject spawnCell = Instantiate(cellPrefab, new Vector3(i, j, 0f), Quaternion.identity);
                CellController spawnCellScript = spawnCell.GetComponent<CellController>();
                spawnCellScript.terrainType = CellController.terrainTypeEnum.Grass;
                spawnCellScript.terrainMagnitude = 5;
                spawnCellScript.xCoord = i;
                spawnCellScript.yCoord = j;

                spawnCellScript.gridContainer = this;

                if (i == 0 || j == 0 || i == gridSizeHorizontal - 1 || j == gridSizeVertical - 1)
                {
                    spawnCellScript.walkable = false;
                }
                else if (Random.Range(0, 100) < 20)
                {
                    spawnCellScript.walkable = false;
                }
                else if (Random.Range(0, 100) < 50)
                {
                    spawnCellScript.terrainMagnitude = Random.Range(0, 4);
                    spawnCellScript.walkable = true;
                }
                else
                {
                    spawnCellScript.walkable = true;
                }

                spawnCellScript.UpdateTexture();



                cells[i, j] = spawnCell;
            }
        }

        if (aStarSetup)
        {
            GameObject spawnUnit = Instantiate(unitPrefab, new Vector3(10, 10, -1f), Quaternion.identity);
            AgentController spawnUnitScript = spawnUnit.GetComponent<AgentController>();

            spawnUnitScript.unitType = AgentController.unitTypeEnum.Sheep;
            spawnUnitScript.health = sheepHealth;
            spawnUnitScript.mentalState = AgentController.mentalStateEnum.Happy;
            spawnUnitScript.UpdateTexture();
            spawnUnitScript.gridContainer = this;

            spawnUnitScript.xCoord = 10;
            spawnUnitScript.yCoord = 10;

            spawnUnitScript.perceptionRange = sheepPerceptionRange;
            spawnUnitScript.urgentRange = sheepUrgentRange;
            
            spawnUnitScript.aStarControlled = true;

            spawnUnitScript.InitialRegister();

            GameObject spawnUnit2 = Instantiate(unitPrefab, new Vector3(12, 12, -1f), Quaternion.identity);
            AgentController spawnUnitScript2 = spawnUnit2.GetComponent<AgentController>();

            spawnUnitScript2.unitType = AgentController.unitTypeEnum.Sheep;
            spawnUnitScript2.health = sheepHealth;
            spawnUnitScript2.mentalState = AgentController.mentalStateEnum.Happy;
            spawnUnitScript2.UpdateTexture();
            spawnUnitScript2.gridContainer = this;

            spawnUnitScript2.xCoord = 12;
            spawnUnitScript2.yCoord = 12;

            spawnUnitScript2.perceptionRange = sheepPerceptionRange;
            spawnUnitScript2.urgentRange = sheepUrgentRange;
            
            spawnUnitScript2.aStarControlled = true;

            spawnUnitScript2.InitialRegister();

            GameObject spawnUnit3 = Instantiate(unitPrefab, new Vector3(13, 11, -1f), Quaternion.identity);
            AgentController spawnUnitScript3 = spawnUnit3.GetComponent<AgentController>();

            spawnUnitScript3.unitType = AgentController.unitTypeEnum.Sheep;
            spawnUnitScript3.health = sheepHealth;
            spawnUnitScript3.mentalState = AgentController.mentalStateEnum.Happy;
            spawnUnitScript3.UpdateTexture();
            spawnUnitScript3.gridContainer = this;

            spawnUnitScript3.xCoord = 13;
            spawnUnitScript3.yCoord = 11;

            spawnUnitScript3.perceptionRange = sheepPerceptionRange;
            spawnUnitScript3.urgentRange = sheepUrgentRange;
            
            spawnUnitScript3.aStarControlled = true;

            spawnUnitScript3.InitialRegister();
        }



        else if (dijkstraSetup)
        {
            GameObject spawnUnit = Instantiate(unitPrefab, new Vector3(10, 10, -1f), Quaternion.identity);
            AgentController spawnUnitScript = spawnUnit.GetComponent<AgentController>();

            spawnUnitScript.unitType = AgentController.unitTypeEnum.Sheep;
            spawnUnitScript.health = sheepHealth;
            spawnUnitScript.mentalState = AgentController.mentalStateEnum.Happy;
            spawnUnitScript.UpdateTexture();
            spawnUnitScript.gridContainer = this;

            spawnUnitScript.xCoord = 10;
            spawnUnitScript.yCoord = 10;

            spawnUnitScript.perceptionRange = sheepPerceptionRange;
            spawnUnitScript.urgentRange = sheepUrgentRange;
            
            spawnUnitScript.dijControlled = true;

            spawnUnitScript.InitialRegister();

            GameObject spawnUnit2 = Instantiate(unitPrefab, new Vector3(12, 12, -1f), Quaternion.identity);
            AgentController spawnUnitScript2 = spawnUnit2.GetComponent<AgentController>();

            spawnUnitScript2.unitType = AgentController.unitTypeEnum.Sheep;
            spawnUnitScript2.health = sheepHealth;
            spawnUnitScript2.mentalState = AgentController.mentalStateEnum.Happy;
            spawnUnitScript2.UpdateTexture();
            spawnUnitScript2.gridContainer = this;

            spawnUnitScript2.xCoord = 12;
            spawnUnitScript2.yCoord = 12;

            spawnUnitScript2.perceptionRange = sheepPerceptionRange;
            spawnUnitScript2.urgentRange = sheepUrgentRange;
            
            spawnUnitScript2.dijControlled = true;

            spawnUnitScript2.InitialRegister();

            GameObject spawnUnit3 = Instantiate(unitPrefab, new Vector3(13, 11, -1f), Quaternion.identity);
            AgentController spawnUnitScript3 = spawnUnit3.GetComponent<AgentController>();

            spawnUnitScript3.unitType = AgentController.unitTypeEnum.Sheep;
            spawnUnitScript3.health = sheepHealth;
            spawnUnitScript3.mentalState = AgentController.mentalStateEnum.Happy;
            spawnUnitScript3.UpdateTexture();
            spawnUnitScript3.gridContainer = this;

            spawnUnitScript3.xCoord = 13;
            spawnUnitScript3.yCoord = 11;

            spawnUnitScript3.perceptionRange = sheepPerceptionRange;
            spawnUnitScript3.urgentRange = sheepUrgentRange;
            
            spawnUnitScript3.dijControlled = true;

            spawnUnitScript3.InitialRegister();
        }

        else
        {
            for (int i = 0; i < unitList.Count; i++)
            {
                if (unitList[i] == unitEnum.Sheep)
                {
                    GameObject spawnUnit = Instantiate(unitPrefab, new Vector3(unitCoordListHoriz[i], unitCoordListVert[i], -1f), Quaternion.identity);
                    AgentController spawnUnitScript = spawnUnit.GetComponent<AgentController>();

                    spawnUnitScript.unitType = AgentController.unitTypeEnum.Sheep;
                    spawnUnitScript.health = sheepHealth;
                    spawnUnitScript.mentalState = AgentController.mentalStateEnum.Happy;
                    spawnUnitScript.UpdateTexture();
                    spawnUnitScript.gridContainer = this;

                    spawnUnitScript.xCoord = unitCoordListHoriz[i];
                    spawnUnitScript.yCoord = unitCoordListVert[i];

                    spawnUnitScript.perceptionRange = sheepPerceptionRange;
                    spawnUnitScript.urgentRange = sheepUrgentRange;

                    spawnUnitScript.InitialRegister();
                }
                else if (unitList[i] == unitEnum.Wolf)
                {
                    GameObject spawnUnit = Instantiate(unitPrefab, new Vector3(unitCoordListHoriz[i], unitCoordListVert[i], -1f), Quaternion.identity);
                    AgentController spawnUnitScript = spawnUnit.GetComponent<AgentController>();

                    spawnUnitScript.unitType = AgentController.unitTypeEnum.Wolf;
                    spawnUnitScript.health = wolfHealth;
                    spawnUnitScript.currentHealth = wolfHealth;
                    spawnUnitScript.hunger = 1;
                    spawnUnitScript.UpdateTexture();
                    spawnUnitScript.gridContainer = this;

                    spawnUnitScript.xCoord = unitCoordListHoriz[i];
                    spawnUnitScript.yCoord = unitCoordListVert[i];

                    spawnUnitScript.perceptionRange = wolfPerceptionRange;
                    spawnUnitScript.urgentRange = wolfUrgentRange;
                    
                    spawnUnitScript.InitialRegister();
                }
            }
        }
    }



    public GameObject GetCellByCoordinate(int x, int y)
    {
        return cells[x, y];
    }

    public void CreateNewSheepAt(int x, int y, int oldSheepHealth)
    {
        if (GetCellByCoordinate(x, y).GetComponent<CellController>().AreYouOccupied() == false && GetCellByCoordinate(x, y).GetComponent<CellController>().AreYouWalkable() == true)
        {
            GameObject spawnUnit = Instantiate(unitPrefab, new Vector3(x, y, -1f), Quaternion.identity);
            AgentController spawnUnitScript = spawnUnit.GetComponent<AgentController>();

            spawnUnitScript.unitType = AgentController.unitTypeEnum.Sheep;
            spawnUnitScript.health = sheepHealth;
            spawnUnitScript.currentHealth = (int) (oldSheepHealth/4);
            spawnUnitScript.mentalState = AgentController.mentalStateEnum.Happy;
            spawnUnitScript.UpdateTexture();
            spawnUnitScript.gridContainer = this;

            spawnUnitScript.xCoord = x;
            spawnUnitScript.yCoord = y;

            spawnUnitScript.perceptionRange = sheepPerceptionRange;
            spawnUnitScript.urgentRange = sheepUrgentRange;

            spawnUnitScript.InitialRegister();
        }
    }

    //Create a 2D array for the grid

    //Instantiate the grid prefab at each coordinate
}
