﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class UtilityCell
{
    public int terrainType;
    public int terrainMagnitude;
    public GameObject registeredUnit;
    public GameObject claimingUnit;

    public int moveCost;

}