﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class AStarCell
{
    public int f;
    public int g;
    public int h;

    public GameObject parentCell;
}